# click-to-dial-application-task
#### Google summer of code Debian project "Click To Dial Popup Window for the Linux Desktop" application task .

Install PyQt5 
```
sudo apt install python3-pyqt5
```

```
git clone https://salsa.debian.org/sanjaypra555-guest/click-to-dial-application-task
cd click-to-dial-application-task
chmod +x primary.py
./primary.py "tel:+91 9988776655"
```
### Screenshot
![Screenshot #1](screenshot/scr.png "Screenshot #1")