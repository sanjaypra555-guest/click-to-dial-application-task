#!/usr/bin/env python3
import sys
from PyQt5.QtWidgets import (QWidget, QApplication, QPushButton,
                             QLabel, QHBoxLayout, QVBoxLayout)
from PyQt5.QtGui import (QPixmap, QIcon, QFont)


class Window(QWidget):

    def __init__(self):
        super().__init__()
        self.parsed_number = sys.argv[1]  # The argument passed from command line
        self.format_telephone_number()
        self.init_ui()

    def init_ui(self):
        """Initialize the first popup window"""
        font = QFont()
        font.setPointSize(24)

        self.label_country_flag = QLabel()
        self.set_country_flag()
        self.label_country_code = QLabel(self.country_code)
        self.label_country_code.setFont(font)
        self.label_telephone_no = QLabel(self.tel_number)
        self.label_telephone_no.setFont(font)

        h_box_of_labels = QHBoxLayout()
        h_box_of_labels.setSpacing(20)
        h_box_of_labels.addStretch()
        h_box_of_labels.addWidget(self.label_country_flag)
        h_box_of_labels.addWidget(self.label_country_code)
        h_box_of_labels.addWidget(self.label_telephone_no)
        h_box_of_labels.addStretch()

        self.button_call = QPushButton()
        pixmap = QPixmap("icons/call.png")
        self.button_call.setIcon(QIcon(pixmap))
        self.button_call.setFixedHeight(50)
        self.button_call.setFixedWidth(80)

        self.button_add_to_contacts = QPushButton()
        pixmap = QPixmap("icons/add_to_contacts.png")
        self.button_add_to_contacts.setIcon(QIcon(pixmap))
        self.button_add_to_contacts.setFixedHeight(50)
        self.button_add_to_contacts.setFixedWidth(80)

        self.button_search_email = QPushButton()
        pixmap = QPixmap("icons/search_email.png")
        self.button_search_email.setIcon(QIcon(pixmap))
        self.button_search_email.setFixedHeight(50)
        self.button_search_email.setFixedWidth(80)

        self.button_call_with_skype = QPushButton()
        pixmap = QPixmap("icons/call_with_skype.png")
        self.button_call_with_skype.setIcon(QIcon(pixmap))
        self.button_call_with_skype.setFixedHeight(50)
        self.button_call_with_skype.setFixedWidth(80)

        h_box_of_buttons = QHBoxLayout()
        h_box_of_buttons.addWidget(self.button_call)
        h_box_of_buttons.addWidget(self.button_add_to_contacts)
        h_box_of_buttons.addWidget(self.button_search_email)
        h_box_of_buttons.addWidget(self.button_call_with_skype)

        v_box = QVBoxLayout()
        v_box.addLayout(h_box_of_labels)
        v_box.addLayout(h_box_of_buttons)

        self.resize(500, 200)
        self.setLayout(v_box)
        self.setWindowTitle("Select Option ...")
        self.show()

    def format_telephone_number(self):
        """Method for validating, formatting and extracting data from telephone number"""

        # this is a dummy phone number formatter , in actual code phonenumber
        # library will be used for validating, formatting and extracting data
        # from parsed telephone number
        tel = self.parsed_number
        tel = tel[4:]
        self.country_code, self.tel_number = tel.split(' ')

    def set_country_flag(self):
        """Method for finding country flag using country code"""

        # this is a dummy country flag finder, in actual code proper library
        # will be used for finding country flag using country code
        self.label_country_flag.setPixmap(QPixmap("icons/india_flag.png"))


app = QApplication(sys.argv)
window = Window()
sys.exit(app.exec_())
